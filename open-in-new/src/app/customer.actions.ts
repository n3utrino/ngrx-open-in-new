import {createAction, props} from '@ngrx/store';

export const loadCustomers = createAction(
  '[Customer] Load Customers'
);

export const openNewTab = createAction(
  '[Customer] Open New Tab', props<{ url: string, settings: { [key: string]: unknown } }>()
);

export const settingsChanged = createAction(
  '[Customer] Customers settings changed', props<{ filter: string | undefined }>()
);


export const settingsSaveSuccess = createAction(
  '[Settings API] save success'
);

export const settingsLoadSuccess = createAction(
  '[Settings API] load success', props<{ filter: string | undefined }>()
);


