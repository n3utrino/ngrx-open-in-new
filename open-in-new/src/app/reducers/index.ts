import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector, createReducer,
  createSelector,
  MetaReducer, on
} from '@ngrx/store';
import {environment} from '../../environments/environment';
import {routerReducer, RouterState} from "@ngrx/router-store";
import {settingsChanged, settingsLoadSuccess} from "../customer.actions";

interface CustomersState {
  filter?: string;
  selectedCustomer?: string;
  customers: { name: string, id: string }[];
}

export interface State {
  router: RouterState
  customers: CustomersState
}

const initialCustomerState: CustomersState = {filter: 'default', customers: [{id: '1', name: 'The First'}]}

export const customersReducer = createReducer(
  initialCustomerState,
  on(settingsChanged, settingsLoadSuccess, (state: CustomersState, {filter}) => ({...state, filter}))
);

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
  customers: customersReducer
};

const selectCustomers = createFeatureSelector<CustomersState>('customers');
export const selectCustomersList = createSelector(selectCustomers, ({customers}) => ({customers}))
export const selectCustomersViewSettings = createSelector(selectCustomers, ({filter}) => ({
  filter
}));

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
