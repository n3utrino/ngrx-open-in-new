import { createAction, props } from '@ngrx/store';

export const loadCustomerApis = createAction(
  '[CustomerApi] Load CustomerApis'
);

export const loadCustomerApisSuccess = createAction(
  '[CustomerApi] Load CustomerApis Success',
  props<{ data: any }>()
);

export const loadCustomerApisFailure = createAction(
  '[CustomerApi] Load CustomerApis Failure',
  props<{ error: any }>()
);
