import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {loadCustomers, openNewTab, settingsChanged, settingsLoadSuccess, settingsSaveSuccess} from "./customer.actions";
import {of, switchMap, tap} from "rxjs";


@Injectable()
export class SettingsEffects {

  share$ = createEffect(() => this.actions$.pipe(
    ofType(openNewTab),
    tap(({url, settings}) => {
      const proxy = window.open(url)
      if (proxy) {
        Object.keys(settings).forEach((key) => {
          proxy.sessionStorage.setItem(key, JSON.stringify(settings[key]));

        })
      }
    })
  ), {dispatch: false});

  save$ = createEffect(() => this.actions$.pipe(
    ofType(settingsChanged),
    switchMap(({filter}) => {
      if (filter) {
        window.sessionStorage.setItem('customerViewSettingsFilter', filter)
      }
      return of(settingsSaveSuccess())
    })
  ))

  load$ = createEffect(() => this.actions$.pipe(
    ofType(loadCustomers),
    switchMap(() => {
      let filter = window.sessionStorage.getItem('customerViewSettingsFilter') || undefined
      return of(settingsLoadSuccess({filter}))
    })
  ))

  constructor(private actions$: Actions) {
  }
}
