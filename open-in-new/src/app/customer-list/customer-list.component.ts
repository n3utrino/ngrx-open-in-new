import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {loadCustomers, openNewTab, settingsChanged} from "../customer.actions";
import {selectCustomersList, selectCustomersViewSettings} from "../reducers";
import {debounce, debounceTime, Observable} from "rxjs";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {

  public filterInput = new FormControl<string | undefined>("");
  public customers$: Observable<{ customers: { name: string, id: string }[] }>;

  constructor(private store: Store) {
    store.select(selectCustomersViewSettings).subscribe((settings) => {
      this.filterInput.setValue(settings.filter, {emitEvent: false});
    });
    this.customers$ = store.select(selectCustomersList);
    this.filterInput.valueChanges.pipe(debounceTime(300)).subscribe((value) => {
      if (value !== null) {
        store.dispatch(settingsChanged({filter: value}))
      }
    });
  }

  public openNew() {
    this.store.dispatch(openNewTab({
      url: '/customers',
      settings: {
        customerViewSettingsFilter: this.filterInput.value
      }
    }))
  }

  ngOnInit(): void {
    this.store.dispatch(loadCustomers())
  }

}
